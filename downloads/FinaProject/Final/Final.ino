#include "U8glib.h"


U8GLIB_ST7920_128X64 u8g( A3, A2, A1, A0);

//ResetButton
#define BUTTON_PIN PB0

//LEDs
#define LED_PIN 12
#define LED_PIN2 13

//Switches
#define ENDSTOP_20_PIN 
#define ENDSTOP_50_PIN 
#define ENDSTOP_1_PIN  11 
#define ENDSTOP_2_PIN  18

//Display
int linespacing;

//Münzen 
int zwanzigCent = 0;
int fuenfzigCent = 0;
int einEuro = 0;
int zweiEuro = 0;
int gesamtGeld = 0;

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers

unsigned long lastDebounceTime2 = 0;  // the last time the output pin was toggled
unsigned long debounceDelay2 = 50;    // the debounce time; increase if the output flickers

int ledState = HIGH;         // the current state of the output pin
int ledState2 = HIGH; 
int buttonState;             // the current reading from the input pin
int lastButtonState = LOW;   // the previous reading from the input pin

int buttonState2;             // the current reading from the input pin
int lastButtonState2 = LOW;   // the previous reading from the input pin

void setup() {
   
  // put your setup code here, to run once:
  // assign default color value
  if ( u8g.getMode() == U8G_MODE_R3G3B2 )
    u8g.setColorIndex(255); // white
  else if ( u8g.getMode() == U8G_MODE_GRAY2BIT )
    u8g.setColorIndex(3); // max intensity
  else if ( u8g.getMode() == U8G_MODE_BW )
    u8g.setColorIndex(1); // pixel on

  //Set Button and LEDs pinMode
  pinMode(LED_PIN,OUTPUT);
  pinMode(LED_PIN2,OUTPUT);
  pinMode(ENDSTOP_1_PIN,INPUT);
  pinMode(ENDSTOP_2_PIN,INPUT);


  //Aktiviere Kontroll LED
  digitalWrite(LED_PIN2, ledState2);
  digitalWrite(LED_PIN, ledState);
  
  Serial.begin(115200);
}

void draw(void) {
  // graphic commands to redraw the complete screen should be placed here  
  //u8g.setFont(u8g_font_osb21);
  u8g.setFont(u8g_font_04b_03b);
  
  linespacing = u8g.getFontLineSpacing();
  // x , y
  
  //Ausgabe Überschrift
  u8g.drawStr( 0, 10, "COIN SORTING BOX - PT");

  //Ausgabe Trennlinie
  u8g.drawStr( 0, 19, "---------------------------------------- "); 
  
  //Ausgabe 20Cent
  u8g.drawStr( 0, (linespacing * 4), "0,20");
  u8g.drawStr( 20, (linespacing * 4), "EUR:");
  char zwanzig[5];
  sprintf(zwanzig, "x %d", zwanzigCent);
  u8g.drawStr( 40,(linespacing * 4), zwanzig);                      

  //Ausgabe 50Cent
  u8g.drawStr( 0, (linespacing * 5), "0,50");
  u8g.drawStr( 20, (linespacing * 5), "EUR:");
  char fuenfzig[4];
  sprintf(fuenfzig, "x %d", fuenfzigCent);
  u8g.drawStr( 40,(linespacing * 5), fuenfzig);                

  //Ausgabe 1Euro
  u8g.drawStr( 0, (linespacing * 6), "1");
   u8g.drawStr( 20, (linespacing * 6), "EUR:");
  char eins[4];
  sprintf(eins, "x %d", einEuro);
  u8g.drawStr( 40,(linespacing * 6), eins);                

  //Ausgabe 2Euro
  u8g.drawStr( 0, (linespacing * 7), "2");
   u8g.drawStr( 20, (linespacing * 7), "EUR:");
  char zwei[4];
  sprintf(zwei, "x %d", zweiEuro);
  u8g.drawStr( 40,(linespacing * 7), zwei);                

  //Ausgabe Trennlinie
  u8g.drawStr( 0, 47, "---------------------------------------- "); 

  //Ausgabe Gesamtbetrag
  u8g.drawStr( 5, (linespacing * 9), "GESAMT:");
  char gesamt[5];
  sprintf(gesamt, "x %d €", gesamtGeld);
  u8g.drawStr( 40,(linespacing * 9), gesamt);  

  
}

void loop() {

  checkSwitch(); 
}

void checkSwitch(){

  int reading = digitalRead(ENDSTOP_1_PIN);
  int reading2 = digitalRead(ENDSTOP_2_PIN);

 //1EURO
 if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }
  
  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != buttonState) {
      buttonState = reading;

      // only toggle the LED if the new button state is HIGH
      if (buttonState == HIGH) {
        ledState = !ledState;
      }
    }
  }

  // set the LED:
  digitalWrite(LED_PIN, ledState);

  einEuro = einEuro + 1;
  
  // save the reading. Next time through the loop, it'll be the lastButtonState:
  lastButtonState2 = reading2;
 
 if (reading2 != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }

  //2EURO
  if ((millis() - lastDebounceTime2) > debounceDelay2) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading2 != buttonState2) {
      buttonState2 = reading2;

      // only toggle the LED if the new button state is HIGH
      if (buttonState2 == HIGH) {
        ledState2 = !ledState2;
      }
    }
  }

  // set the LED:
  digitalWrite(LED_PIN2, ledState2);

  zweiEuro = zweiEuro + 1;
  
  // save the reading. Next time through the loop, it'll be the lastButtonState:
  lastButtonState2 = reading2;


  
  //Wenn Endstop 20 getriggert
      //ENDSTOP_1_PIN  
      //1 Euro + 1

  //Wenn Endstop 20 getriggert
      //ENDSTOP_2_PIN  
      //2 Euro + 1

  //Gesamtgeld berechnen
  gesamtGeld = einEuro + (zweiEuro * 2);
}

void resetValues(){
  zwanzigCent = 0;
  fuenfzigCent = 0;
  einEuro = 0;
  zweiEuro = 0;
  gesamtGeld = 0;
}
