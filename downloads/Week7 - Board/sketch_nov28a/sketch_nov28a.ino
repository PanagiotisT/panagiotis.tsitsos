#define LED_PIN 12
#define LED_PIN2 13
#define BUTTON_PIN PB0

bool switchLED = true;
bool buttonText = true;
bool noInput = true;
char launchText[20] = "Succesfully launched";


void setup() {
  // put your setup code here, to run once:
  pinMode(LED_PIN,OUTPUT);
  pinMode(LED_PIN2,OUTPUT);
  pinMode(BUTTON_PIN,INPUT);
  
  Serial.begin(115200);

  for(int i=0; i < 20; i++) {
     Serial.write(launchText[i]);
  }
  Serial.println("");
}

void loop() {
  // put your main code here, to run repeatedly;
  if(Serial.available()) {
    char input = Serial.read();

    if(input == 'x'){
      noInput = false;
      Serial.println("Stopped LED");
    }

    if(input == 'c'){
      noInput = true;
      Serial.println("Started LED");
    } 
}

  if(noInput){
    if( digitalRead(BUTTON_PIN) ) {
        switchLED = !switchLED;
        delay(200);

        if(switchLED){
           Serial.println("LED_PIN active");
        }else{
           Serial.println("LED_PIN2 active");
        }
      }
      
      if(switchLED){
        digitalWrite(LED_PIN, HIGH);
        digitalWrite(LED_PIN2, LOW);
        delay(200);
        
      }else{
        digitalWrite(LED_PIN, LOW);
        digitalWrite(LED_PIN2, HIGH);
        delay(200);
      }    
  }else{
      digitalWrite(LED_PIN, LOW);
      digitalWrite(LED_PIN2, LOW);
       if( digitalRead(BUTTON_PIN) ) {
          buttonText = !buttonText;
          delay(200);

          if(buttonText){
              Serial.println("Type C first to activate the previous button function");
          }else{
             Serial.println("Type C first to activate the previous button function");
          } 
       }
     }
  }
