#include "DHT.h"

#define DHT_PIN 10
#define DHTTYPE DHT11

DHT dht(DHT_PIN, DHTTYPE);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println("Humidity and temperature monitor started!");
  dht.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(3000);

  float h = dht.readHumidity();
  float t = dht.readTemperature();

  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.print(" \t");
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.println("");
}
