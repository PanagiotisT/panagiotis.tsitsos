// Arduino koncový doraz / spínač

// nastavení propojovacího pinu
#define pinD 22
// vytvoření proměnných pro ukládání dat
long casPreruseni;

void setup() {
  // inicializace komunikace po sériové lince
  // rychlostí 9600 baud
  Serial.begin(9600);
  // nastavení přerušení na pin 2 (int0)
  // při nástupné hraně (log0->log1) se vykoná program prerus
  attachInterrupt(digitalPinToInterrupt(pinD), prerus, RISING);
}
void loop() {
  // místo pro běžný běh programu
  // pro ukázku vytiskneme každou sekundu
  // čas od spuštění Arduina a čas
  // poslední detekce dorazu
  Serial.print("Cas od spusteni Arduina: ");
  Serial.print(millis()/1000);
  Serial.print("s; cas posledni detekce: ");
  Serial.print(casPreruseni);
  Serial.println("s.");
  // pauza po dobu 1 sekundy
  delay(1000);
}
void prerus() {
  // uložíme čas detekce dorazu
  casPreruseni = millis()/1000;
  // vypiš varovnou hlášku, pokud je aktivován digitální vstup
  Serial.print("Detekovan doraz!");
  if (digitalRead(pinD) == HIGH) {
    Serial.println(" Spinac je rozpojeny.");
  }
  else {
    Serial.println(" Spinac je spojeny.");
  }
}
