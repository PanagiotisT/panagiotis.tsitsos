$( document ).ready(function() {
    console.log( "ready!" );

    $(".nav").addClass('animated bounceInDown delay-2s');
    $(".nav").removeClass("hidden");

    $(".page-footer").addClass('animated bounceInUp delay-2s');
    $(".page-footer").removeClass("hidden");

    setTimeout(function(){
        $("#title").addClass('fadeOutLeft');
        }, 2000);
});
