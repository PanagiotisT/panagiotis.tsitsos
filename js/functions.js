$(".disabled-link").click(function(event) {
    event.preventDefault();
  });

  $(document).ready(function () {
    $(".footerLoad").html('<!-- Footer -->' +                   
    '<footer class="page-footer font-small cyan darken-3">' +
    '<!-- Footer Elements -->' +
    '<div class="container">' +
    '<!-- Grid row-->' +
    '<div class="row">' +
    '<!-- Grid column -->' +
    '   <div class="col-md-12 py-3">' +
    ' <div class="flex-center">' +
    ' <!-- fa-2 für die größe des Icons -->' +
    '<!-- Facebook -->' +
    '<a class="fb-ic">' +
    '<i class="fa fa-facebook fa-lg white-text mr-md-3 mr-3 ml-3 fa-2x"> </i> </a>' +
    '<a class="tw-ic"> <i class="fa fa-twitter fa-lg white-text mr-md-3 mr-3 ml-3 fa-2x"> </i></a>' +
    '<a class="ins-ic"><i class="fa fa-instagram fa-lg white-text mr-md-3 mr-3 ml-3 fa-2x"> </i></a>' +
    '</div></div> </div></div>' +
    '<div class="footer-copyright text-center py-3">© 2018 Copyright: Panagiotis Tsitsos</div></footer>');

    $(".NavbarLoad").html('<ul class="nav justify-content-center cyan darken-3 py-4">' +
       '<li class="nav-item">' +
          '<a class="nav-link text-white active" href="home.html">Home</a>' +
        '</li>  ' +    
        '<li class="nav-item dropdown">                                                                                                                                 ' +
            '<a class="nav-link dropdown-toggle text-white" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Assignments</a>' +
            '<div class="dropdown-menu">' +
             '<a class="dropdown-item" href="week1.html">Week 1 - Project Management</a>' +
              '<a class="dropdown-item" href="week2.html">Week 2 - 2D and 3D Design</a>' +
              '<a class="dropdown-item" href="week3.html">Week 3 - Laser Cutting</a>' +
              '<!-- <div class="dropdown-divider"></div> -->' +
              '<a class="dropdown-item" href="week4.html">Week 4 - 3D Printing</a>' +
              '<a class="dropdown-item" href="week5.html">Week 5 - Electronics production</a>' +
              '<a class="dropdown-item" href="week6.html">Week 6 - Electronics design</a>' +
              '<a class="dropdown-item" href="week7.html">Week 7 - Embedded programming</a>' +
              '<a class="dropdown-item" href="week8.html">Week 8 - Input devices</a>' +
              '<a class="dropdown-item" href="week9.html">Week 9 - Output devices</a>' +
              '<a class="dropdown-item" href="finalProject.html">Final Project</a>' +
            '</div> '+
          '</li> ' +
    
    '</ul>');
  });